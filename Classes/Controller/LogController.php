<?php
namespace HIVE\HiveExtFeloginTracking\Controller;

use HIVE\HiveExtFeloginTracking\Domain\Model\Log;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Service\TypoLinkCodecService;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
/***
 *
 * This file is part of the "hive_ext_felogin_tracking" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * LogController
 */
class LogController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * logRepository
     *
     * @var \HIVE\HiveExtFeloginTracking\Domain\Repository\LogRepository
     * @inject
     */
    protected $logRepository = null;

    /**
     * frontendUserGroupRepository
     *
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository = null;

    /**
     * frontendUserRepository
     *
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $logs = $this->logRepository->findAll();
        /*
         * We want to show a list of "Logs" only in the storage folders
         * so we need to know the actual page uid in the backend.
         *
         * Then we use this uid as storagePid in the repo :).
         */
        if (TYPO3_MODE === 'BE') {
            $iCurrentPageId = 0;
            if ($GLOBALS['TSFE'] === NULL){
                $iCurrentPageId = (int) \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('id');
            }
            $this->view->assign('iCurrentPageId', $iCurrentPageId);
            $logs = $this->logRepository->findAll($iCurrentPageId);
        }

        $this->view->assign('logs', $logs);

    }

    /**
     * action log
     *
     * @return void
     */
    public function logAction()
    {
        $aCurrentFrontendUser = $GLOBALS['TSFE']->fe_user->user;
        if (isset($aCurrentFrontendUser) and count($aCurrentFrontendUser) > 0 and isset($aCurrentFrontendUser['uid']) and isset($aCurrentFrontendUser['username'])) {
            /*
             * Get UserGroup title
             */

            $aFrontendUserGroup = [];
            $aFrontendUserGroup['uid'] = $aCurrentFrontendUser['usergroup'];
            $aFrontendUserGroup['title'] = $this->frontendUserGroupRepository->findByUid($aCurrentFrontendUser['usergroup'])->getTitle();
            /*
             * Get User title
             */

            $aFrontendUser = [];
            $aFrontendUser['uid'] = $aCurrentFrontendUser['uid'];
            $aFrontendUser['title'] = $aCurrentFrontendUser['username'];

            /*
             * Get Current Page
             */
            $sUri = '';
            $iPageUid = $GLOBALS['TSFE']->id;
            $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
            $sUri = $contentObject->typoLink_URL(
                [
                    'parameter' => self::createTypolinkParameterFromArguments($iPageUid),
                    'useCacheHash' => TRUE,
                    'forceAbsoluteUrl' => FALSE,
                ]
            );

            $oLog = new Log();
            $oLog->setUsergroup($aFrontendUserGroup['uid']);
            $oLog->setUsergroupTitle($aFrontendUserGroup['title']);
            $oLog->setUser($aFrontendUser['uid']);
            $oLog->setUserTitle($aFrontendUser['title']);
            $oLog->setPage($iPageUid);
            $oLog->setPagePath($sUri);
            $oLog->setAccess(new \DateTime('now'));
            $this->logRepository->add($oLog);
        }
    }

    /**
     * Transforms ViewHelper arguments to typo3link.parameters.typoscript option as array.
     *
     * @param string $parameter Example: 19 _blank - "testtitle with whitespace" &X=y
     * @param string $additionalParameters
     *
     * @return string The final TypoLink string
     */
    protected static function createTypolinkParameterFromArguments($parameter, $additionalParameters = '')
    {
        $typoLinkCodec = GeneralUtility::makeInstance(TypoLinkCodecService::class);
        $typolinkConfiguration = $typoLinkCodec->decode($parameter);

        // Combine additionalParams
        if ($additionalParameters) {
            $typolinkConfiguration['additionalParams'] .= $additionalParameters;
        }

        return $typoLinkCodec->encode($typolinkConfiguration);
    }
}
