
plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog {
    view {
        # cat=plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_felogin_tracking/Resources/Private/Templates/
        # cat=plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_felogin_tracking/Resources/Private/Partials/
        # cat=plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_felogin_tracking/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog//a; type=string; label=Default storage PID
        storagePid =
    }
}

module.tx_hiveextfelogintracking_hiveextfelogintracking {
    view {
        # cat=module.tx_hiveextfelogintracking_hiveextfelogintracking/file; type=string; label=Path to template root (BE)
        templateRootPath = EXT:hive_ext_felogin_tracking/Resources/Private/Backend/Templates/
        # cat=module.tx_hiveextfelogintracking_hiveextfelogintracking/file; type=string; label=Path to template partials (BE)
        partialRootPath = EXT:hive_ext_felogin_tracking/Resources/Private/Backend/Partials/
        # cat=module.tx_hiveextfelogintracking_hiveextfelogintracking/file; type=string; label=Path to template layouts (BE)
        layoutRootPath = EXT:hive_ext_felogin_tracking/Resources/Private/Backend/Layouts/
    }
    persistence {
        # cat=module.tx_hiveextfelogintracking_hiveextfelogintracking//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder