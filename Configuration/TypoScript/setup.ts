
plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog {
    view {
        templateRootPaths.0 = EXT:hive_ext_felogin_tracking/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_felogin_tracking/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_felogin_tracking/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hiveextfelogintracking._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-ext-felogin-tracking table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-ext-felogin-tracking table th {
        font-weight:bold;
    }

    .tx-hive-ext-felogin-tracking table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

# Module configuration
module.tx_hiveextfelogintracking_web_hiveextfelogintrackinghiveextfelogintracking {
    persistence {
        storagePid = {$module.tx_hiveextfelogintracking_hiveextfelogintracking.persistence.storagePid}
    }
    view {
        templateRootPaths.0 = EXT:hive_ext_felogin_tracking/Resources/Private/Backend/Templates/
        templateRootPaths.1 = {$module.tx_hiveextfelogintracking_hiveextfelogintracking.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_felogin_tracking/Resources/Private/Backend/Partials/
        partialRootPaths.1 = {$module.tx_hiveextfelogintracking_hiveextfelogintracking.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_felogin_tracking/Resources/Private/Backend/Layouts/
        layoutRootPaths.1 = {$module.tx_hiveextfelogintracking_hiveextfelogintracking.view.layoutRootPath}
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

##
## Log :: Log
##
lib.tx_hiveextfelogintracking_hiveextfelogintrackinglog = COA
lib.tx_hiveextfelogintracking_hiveextfelogintrackinglog {
    10 = USER
    10 {
        userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
        extensionName = HiveExtFeloginTracking
        pluginName = Hiveextfelogintrackinglog
        vendorName = HIVE
        controller = Log
        action = log
        settings =< plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog.settings
        persistence =< plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog.persistence
        view =< plugin.tx_hiveextfelogintracking_hiveextfelogintrackinglog.view
    }
}

##
## .:: USE THE LIB AS SHOWN BELOW ::.
##
## .:: Add to Setup ::.
##
## lib.trackusers = COA
## lib.trackusers.10 =< lib.tx_hiveextfelogintracking_hiveextfelogintrackinglog.10
## [PIDinRootline = 13]
## [else]
## lib.trackusers = COA
## lib.trackusers.10 >
## [global]
##
##
## .:: Add to Template ::.
##
## <f:cObject typoscriptObjectPath="lib.trackusers" />
##