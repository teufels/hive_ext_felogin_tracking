<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtFeloginTracking',
            'Hiveextfelogintrackinglog',
            [
                'Log' => 'log'
            ],
            // non-cacheable actions
            [
                'Log' => 'log'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hiveextfelogintrackinglog {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_felogin_tracking') . 'Resources/Public/Icons/user_plugin_hiveextfelogintrackinglog.svg
                        title = LLL:EXT:hive_ext_felogin_tracking/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_felogin_tracking_domain_model_hiveextfelogintrackinglog
                        description = LLL:EXT:hive_ext_felogin_tracking/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_felogin_tracking_domain_model_hiveextfelogintrackinglog.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextfelogintracking_hiveextfelogintrackinglog
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder